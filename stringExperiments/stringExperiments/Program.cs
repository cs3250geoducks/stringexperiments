﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace stringExperiments
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "this is a string second.";
            string s2 = "this is a bunch of counting second.";

            string[] s1Array = s1.Split(' ');
            string[] s2Array = s2.Split(' ');

            StringComparer stringComparer = StringComparer.Create(CultureInfo.CurrentCulture, false);

            List<int> diffIndecies = new List<int>();
            int index = -1;
            int length = s2Array.Length;
            foreach (string x in s1Array)
            {

                index++;

                while (length > index && !stringComparer.Equals(x, s2Array.ElementAt(index)))
                {
                    diffIndecies.Add(index);
                    index++;
                }

                if (length < index)
                {
                    break;
                }

            }
        }
    }
}
